package simpledb

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

func fileExist(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func writeJson(path string, any interface{}) {
	b, _ := json.Marshal(any)
	s := string(b)
	writeFile(path, s)
}

func readJson(path string, any interface{}) {

	s := readFile(path)
	_ = json.Unmarshal([]byte(s), any)
}

func writeFile(path string, content string) {
	err := ioutil.WriteFile(path, []byte(content), 0644)
	if err != nil {
		log.Println("write error", err)
	}
}

func deleteFile(path string) {
	os.Remove(path)
}

func readFile(path string) (s string) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println(err)
		return
	}
	s = string(b)
	return s
}
