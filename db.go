package simpledb

type Data struct {
	Partition string
	key       string
}

const f = "data/"

func Make(part string, key string) Data {
	return Data{part, key}
}

func (d *Data) ChangeKey(key string) Data {
	d.key = key
	return *d
}

func (d *Data) partFile() string {
	return f + d.Partition + ".part"
}

func (d *Data) objectFile() string {
	return f + d.Partition + "_" + d.key + ".json"
}

func (*Data) masterFile() string {
	return f + "master.master"
}

func (d *Data) GetPartitions() map[string]bool {
	if !fileExist(d.masterFile()) {
		m := make(map[string]bool)
		writeJson(d.masterFile(), m)
	}
	var master map[string]bool
	readJson(d.masterFile(), &master)
	return master
}

func (d *Data) GetPartitionArray() []string {
	part := d.GetPartitions()
	v := make([]string, 0, len(part))
	for val := range part {
		v = append(v, val)
	}
	return v
}

func (d *Data) addPartition() {

	parts := d.GetPartitions()
	parts[d.Partition] = true
	writeJson(d.masterFile(), parts)

	empty := make(map[string]bool)
	writeJson(d.partFile(), empty)

}

func (d *Data) ExistPartition() bool {
	partitions := d.GetPartitions()
	if _, ok := partitions[d.Partition]; ok {
		return fileExist(d.partFile())
	} else {
		return false
	}
}

func (d *Data) GetKeys() map[string]bool {
	var keys map[string]bool
	readJson(d.partFile(), &keys)
	return keys
}

func (d *Data) GetKeyArray() []string {
	key := d.GetKeys()
	v := make([]string, 0, len(key))
	for val := range key {
		v = append(v, val)
	}
	return v

}

func (d *Data) Exist() bool {
	if d.ExistPartition() {
		keys := d.GetKeys()
		if _, ok := keys[d.key]; ok {
			return fileExist(d.objectFile())
		} else {
			return false
		}
	} else {
		return false
	}
}

func (d *Data) Delete() {
	if !d.ExistPartition() {
		d.addPartition()
	}
	if !d.Exist() {
		keys := d.GetKeys()
		if _, ok := keys[d.key]; ok {
			delete(keys, d.key)
		}
		writeJson(d.partFile(), keys)
	}
	deleteFile(d.objectFile())
}

func (d *Data) Save(val interface{}) {
	if !d.ExistPartition() {
		d.addPartition()
	}
	if !d.Exist() {
		keys := d.GetKeys()
		keys[d.key] = true
		writeJson(d.partFile(), keys)
	}
	writeJson(d.objectFile(), val)
}

func (d *Data) Read(val interface{}) {
	readJson(d.objectFile(), val)
}
